import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:flutter/material.dart';
import 'package:test_task/pages/hero_page.dart';
import 'package:test_task/pages/steps_page.dart';
import 'package:test_task/widgets/navbar.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Navbar(),
          Expanded(
            child: ListView(
              children: [
                const HeroPage(),
                const StepsPage(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
