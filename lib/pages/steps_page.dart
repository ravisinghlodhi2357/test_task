import 'package:flutter/material.dart';

class StepsPage extends StatefulWidget {
  const StepsPage({super.key});

  @override
  State<StepsPage> createState() => _StepsPageState();
}

class _StepsPageState extends State<StepsPage> {
  int selectedIndex = 0;
  final _textStyle = const TextStyle(color: Colors.white);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 80),
      alignment: Alignment.center,
      child: Container(
        child: Column(
          children: [
            _buttonBar(),
            const SizedBox(height: 40),
            const Text(
              'Drei einfache Schritte\nzu deinem neuen Job',
              style: TextStyle(
                fontSize: 24,
              ),
            ),
            SizedBox(width: 1100, child: _section1()),
            _section2(),
            SizedBox(width: 1100, child: _section3()),
          ],
        ),
      ),
    );
  }

  Widget _section1() {
    return SizedBox(
      height: 400,
      child: Row(
        children: [
          Container(
            height: 200,
            width: 500,
            child: Stack(
              children: [
                Positioned(
                  child: CircleAvatar(
                    radius: 60,
                    backgroundColor: Colors.grey[100],
                  ),
                  top: 50,
                  left: 100,
                ),
                Positioned(
                  top: 80,
                  left: 120,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '1.',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                          color: Colors.grey,
                          height: 1.5,
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Text(
                          'Erstellen dein Lebenslauf',
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 200,
            width: 200,
            color: Colors.grey[100],
            alignment: Alignment.center,
            child: Text('Image'),
          ),
        ],
      ),
    );
  }

  Widget _section2() {
    return Stack(
      children: [
        Positioned(
          child: Image.asset(
            'images/bg_gradient.png',
            fit: BoxFit.fill,
          ),
        ),
        SizedBox(
          height: 400,
          width: 1100,
          child: Row(
            children: [
              const Spacer(),
              Container(
                  height: 200,
                  width: 200,
                  color: Colors.grey[100],
                  alignment: Alignment.center,
                  child: const Text('Image')),
              Container(
                margin: const EdgeInsets.only(left: 50),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Text(
                      '2.',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 40,
                        color: Colors.grey,
                        height: 1.5,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: const Text(
                        'Erstellen dein Lebenslauf',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _section3() {
    return SizedBox(
      height: 400,
      child: Row(
        children: [
          SizedBox(
            height: 200,
            width: 500,
            child: Stack(
              children: [
                Positioned(
                  child: CircleAvatar(
                    radius: 60,
                    backgroundColor: Colors.grey[100],
                  ),
                  top: 50,
                  left: 100,
                ),
                Positioned(
                  top: 80,
                  left: 120,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Text(
                        '3.',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                          color: Colors.grey,
                          height: 1.5,
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: const Text(
                          'Mit nur einem Klick\nbewerben',
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 200,
            width: 200,
            color: Colors.grey[100],
            alignment: Alignment.center,
            child: Text('Image'),
          ),
        ],
      ),
    );
  }

  Row _buttonBar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buildButton(
          text: 'Arbeitnehmer',
          onClick: () {
            setState(() {
              selectedIndex = 0;
            });
          },
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(8),
              bottomLeft: Radius.circular(8),
            ),
            border: Border.all(
              color: Colors.black12,
            ),
            color: getColor(0),
          ),
          textStyle: isSelected(0) ? _textStyle : null,
        ),
        _buildButton(
          text: 'Arbeitgeber',
          onClick: () {
            setState(() {
              selectedIndex = 1;
            });
          },
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.black12,
            ),
            color: getColor(1),
          ),
          textStyle: isSelected(1) ? _textStyle : null,
        ),
        _buildButton(
          text: 'Temporarburo',
          onClick: () {
            setState(() {
              selectedIndex = 2;
            });
          },
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topRight: Radius.circular(8),
              bottomRight: Radius.circular(8),
            ),
            border: Border.all(
              color: Colors.black12,
            ),
            color: getColor(2),
          ),
          textStyle: isSelected(2) ? _textStyle : null,
        ),
      ],
    );
  }

  bool isSelected(int index) {
    return selectedIndex == index;
  }

  Color? getColor(int index) {
    if (selectedIndex == index) return Colors.green;
    return Colors.white;
  }

  GestureDetector _buildButton({
    required BoxDecoration decoration,
    required String text,
    required void Function() onClick,
    TextStyle? textStyle,
  }) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        decoration: decoration,
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 8,
        ),
        child: Text(text, style: textStyle),
      ),
    );
  }
}
