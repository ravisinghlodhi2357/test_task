import 'package:flutter/material.dart';
import 'package:test_task/home_page.dart';
import 'package:test_task/home_page_mobile.dart';
import 'package:test_task/widgets/responsive.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Responsive(
        desktop: HomePage(),
        mobile: HomePageMobile(),
      ),
    );
  }
}
